package mx.cdmx.chess.engine.axolotl;

import static mx.cdmx.chess.engine.utils.ChessUtils.STD_FEN;
import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author CGM
 *
 */
@Slf4j
public class AxolotlEngineTest {

    @Test
    public void testCalculateMove() {
        final AxolotlEngine engine = new AxolotlEngine();
        engine.setup();
        final String move = engine.calculateMove(STD_FEN, 10_000);
        log.debug("Calculated move is: {}", move);
        assertNotNull("", move);
    }

}
