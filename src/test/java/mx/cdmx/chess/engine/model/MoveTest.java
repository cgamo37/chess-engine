package mx.cdmx.chess.engine.model;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import com.github.louism33.chesscore.Chessboard;

/**
 * 
 * @author CGM - 27 may, 2019.
 *
 */
public class MoveTest {

    @Test
    public void testGetPretty() {
        final Move move = new Move("e2e4");
        assertEquals("", "♙e4", move.getPretty(new Chessboard()));
    }

    @Test
    public void testGetPrettyWrongMove() {
        final Move move = new Move("e2e5");
        assertThrows(IllegalArgumentException.class, () -> {
            move.getPretty(new Chessboard());
        });
    }

}
