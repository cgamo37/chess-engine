package mx.cdmx.chess.engine.ui;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * 
 * @author CesarGM
 *
 */
public class ChessControllerTest {
    private static final String MOVES;

    static {
        MOVES = "1. e2e4 e7e5\n2. g1f3 g8f6\n3. b1c3 b8c6\n4. f1c4 f6e4\n"
                + "5. d2d3 e4c3\n6. b2c3 d7d5\n7. c4b3 a7a5\n8. a2a4 c8e6\n9. e1g1 f8d6\n10. c1g5";
// @formatter:off
/*
1. ♙e4 ♟e5
2. ♘f3 ♞f6
3. ♘c3 ♞c6
4. ♗c4 ♞e4
5. ♙d3 ♞c3
6. ♙c3 ♟d5
7. ♗b3 ♟a5
8. ♙a4 ♝e6
9. ♔g1 ♝d6
10. ♗g5
*/
// @formatter:on
    }

    private ChessController controller;

    @Test
    public void testDoBackHalfLastMove() {
        controller = new ChessController(MOVES);
        assertEquals("", 10, controller.getMovCounter());
        controller.doBack();
        assertEquals("", 9, controller.getMovCounter());
    }

    @Test
    public void testDoBackFullLastMove() {
        controller = new ChessController(MOVES.replace("10. c1g5", ""));
        assertEquals("", 9, controller.getMovCounter());
        controller.doBack();
        assertEquals("", 9, controller.getMovCounter());
    }

}
