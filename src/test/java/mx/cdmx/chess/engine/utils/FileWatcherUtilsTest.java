package mx.cdmx.chess.engine.utils;

import java.io.IOException;

import org.junit.jupiter.api.Test;

import uk.org.lidalia.sysoutslf4j.context.SysOutOverSLF4J;

/**
 * 
 * @author gamo_ CGM.
 *
 */
class FileWatcherUtilsTest {

    static {
        SysOutOverSLF4J.sendSystemOutAndErrToSLF4J();
    }

    @Test
    @org.junit.jupiter.api.Disabled("This is an integration test that requires a person to play.")
    void testPlayTheGame() throws IOException, InterruptedException {
        FileWatcherUtils.main();
    }

    @Test
    void testResetBoardFromPGN() {
        FileWatcherUtils.resetBoardFromPGN("g1g2");
    }

}
