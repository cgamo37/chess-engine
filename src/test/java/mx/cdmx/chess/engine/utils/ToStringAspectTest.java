package mx.cdmx.chess.engine.utils;

import static org.junit.Assert.assertEquals;

import org.junit.jupiter.api.Test;

import com.github.louism33.chesscore.Chessboard;

import lombok.extern.slf4j.Slf4j;
import mx.cdmx.chess.engine.model.ChessboardWrapper;

/**
 * 
 * @author CGM.
 *
 */
@Slf4j
@SuppressWarnings({ "PMD.AtLeastOneConstructor" })
class ToStringAspectTest {
    /** Board as string after aspect. */
    private static final String EXPECTED;
    /** FEN to use in test board. */
    private static final String FEN;

    static {
        FEN = "r1b2rk1/5p1p/p1pb4/1p1n1qB1/3P2R1/1BP3P1/PP3P1P/RN1Q2K1 b";
        EXPECTED =
        // @formatter:off
        "   a b c d e f g h\n" + 
        "  +---------------+\n" + 
        "8 |♜ = ♝ = · ♜ ♚ =| 8\n" + 
        "7 |= · = · = ♟ = ♟| 7\n" + 
        "6 |♟ = ♟ ♝ · = · =| 6\n" + 
        "5 |= ♟ = ♞ = ♛ ♗ ·| 5\n" + 
        "4 |· = · ♙ · = ♖ =| 4\n" + 
        "3 |= ♗ ♙ · = · ♙ ·| 3\n" + 
        "2 |♙ ♙ · = · ♙ · ♙| 2\n" + 
        "1 |♖ ♘ = ♕ = · ♔ ·| 1\n" + 
        "  +---------------+\n" + 
        "   a b c d e f g h\n";
        // @formatter:on
    }

    /** Test case for static method getBoard. */
    @Test
    void testGetProxy() {
        // Used FEN is interesting in Marshall gambit.
        final Chessboard board = new Chessboard(FEN);
        final ChessboardWrapper proxy = ToStringAspect.getProxy(board);
        log.info("{}", proxy);
        assertEquals("", EXPECTED, proxy.toString());
    }

}
