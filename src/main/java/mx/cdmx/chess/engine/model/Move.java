package mx.cdmx.chess.engine.model;

import static com.github.louism33.chesscore.BoardConstants.*;

import java.util.HashMap;
import java.util.Map;

import com.github.louism33.chesscore.Chessboard;
import com.github.louism33.chesscore.MoveParser;
import com.github.louism33.utils.MoveParserFromAN;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author gamo_ CGM created at 23 may, 2019.
 * Modified at 19 December, 2019: make validation public static. 
 *
 */
@Data
@Slf4j
public class Move {
    public static final Map<Integer, String> MAP_TO_SYMBOL;
    private final String annotation;

    static {
        MAP_TO_SYMBOL = new HashMap<>();

        MAP_TO_SYMBOL.put(WHITE_PAWN, "♙");
        MAP_TO_SYMBOL.put(WHITE_KNIGHT, "♘");
        MAP_TO_SYMBOL.put(WHITE_BISHOP, "♗");
        MAP_TO_SYMBOL.put(WHITE_ROOK, "♖");
        MAP_TO_SYMBOL.put(WHITE_QUEEN, "♕");
        MAP_TO_SYMBOL.put(WHITE_KING, "♔");
        MAP_TO_SYMBOL.put(BLACK_PAWN, "♟");
        MAP_TO_SYMBOL.put(BLACK_KNIGHT, "♞");
        MAP_TO_SYMBOL.put(BLACK_BISHOP, "♝");
        MAP_TO_SYMBOL.put(BLACK_ROOK, "♜");
        MAP_TO_SYMBOL.put(BLACK_QUEEN, "♛");
        MAP_TO_SYMBOL.put(BLACK_KING, "♚");
    }

    /**
     * Get symbolic representation for chess piece corresponding to this move.
     * 
     * @author gamo_ created at 26 may, 2019.
     * @return Movement 'prettyfied', for instance, if this move represents an
     *         initial move 'e2e4', this method returns '♙e4'.
     */
    public String getPretty(final Chessboard board) {
        final Integer nextMove = MoveParserFromAN.buildMoveFromLAN(board, annotation);
        log.trace("nextMove: {}", nextMove);
        if (validate(board, annotation)) {
            final Integer movingPiece = MoveParser.getMovingPieceInt(nextMove);
            log.debug("movingPiece: {}", movingPiece);
            final String piece = MAP_TO_SYMBOL.get(movingPiece);
            return piece + annotation.substring(annotation.length() - 2);
        } else {
            throw new IllegalArgumentException(annotation + " is not a legal movement for chessboard:\n" + board);
        }
    }

    public static boolean validate(final Chessboard board, final String annotation) {
        final Integer nextMove = MoveParserFromAN.buildMoveFromLAN(board, annotation);
        final int[] moves = board.generateLegalMoves();
        Boolean valid = false;
        for (final int move : moves) {
            if (move != 0) {
                log.trace("legal move: {}", move);
                if (move == nextMove.intValue()) {
                    valid = true;
                    break;
                }
            }
        }
        return valid;
    }

}
