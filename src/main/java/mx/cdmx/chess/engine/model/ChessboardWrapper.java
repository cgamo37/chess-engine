package mx.cdmx.chess.engine.model;

import com.github.louism33.chesscore.Chessboard;

import lombok.Data;

/**
 * 
 * This class is for support of ToStringAspect applied to final class
 * 'com.github.louism33.chesscore.Chessboard'.
 * 
 * @author gamo_ created martes, 28 may 2019
 *
 */
@Data
public class ChessboardWrapper {
    private final Chessboard board;
}
