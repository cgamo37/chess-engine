package mx.cdmx.chess.engine;

public interface ChessEngine {

    Boolean setup();

    void stop();

    String calculateMove(String fen, int timeLimit);

}
