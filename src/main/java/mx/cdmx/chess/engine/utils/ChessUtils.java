package mx.cdmx.chess.engine.utils;

/**
 * 
 * @author gamo_ created at 23 may, 2019
 *
 */
//@SuppressWarnings({ "PMD.LawOfDemeter", "PMD.DataflowAnomalyAnalysis" })
public final class ChessUtils {
	/** Standard initial position in FEN notation. */
	public static final String STD_FEN;

	static {
		/*
		 * A FEN contains six fields. The separator between fields is a space.
		 * 
		 * The fields are:
		 * 
		 * 1. Piece placement on squares (A8 B8 .. G1 H1) Each piece is identified by a
		 * letter taken from the standard English names (white upper-case, black
		 * lower-case). Blank squares are noted using digits 1 through 8 (the number of
		 * blank squares), and "/" separate ranks.
		 * 
		 * 2. Active color. "w" means white moves next, "b" means black.
		 * 
		 * 3. Castling availability. Either - if no side can castle or a letter
		 * (K,Q,k,q) for each side and castle possibility.
		 * 
		 * 4. En passant target square in algebraic notation or "-".
		 * 
		 * 5. Half-move clock: This is the number of half-moves since the last pawn
		 * advance or capture.
		 * 
		 * 6. Full-move number: The number of the current full move.
		 * 
		 */
		STD_FEN = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1";
	}

	private ChessUtils() {
		// This is a utility class.
	}

}
