package mx.cdmx.chess.engine.utils;

import com.github.louism33.chesscore.Chessboard;
import com.github.louism33.utils.MoveParserFromAN;

import lombok.extern.slf4j.Slf4j;
import mx.cdmx.chess.engine.model.Move;

/**
 * 
 * A FEN contains six fields. The separator between fields is a space.
 * 
 * The fields are:
 * 
 * 1. Piece placement on squares (A8 B8 .. G1 H1) Each piece is identified by a
 * letter taken from the standard English names (white upper-case, black
 * lower-case). Blank squares are noted using digits 1 through 8 (the number of
 * blank squares), and "/" separate ranks.
 * 
 * 2. Active color. "w" means white moves next, "b" means black.
 * 
 * 3. Castling availability. Either - if no side can castle or a letter
 * (K,Q,k,q) for each side and castle possibility.
 * 
 * 4. En passant target square in algebraic notation or "-".
 * 
 * 5. Half-move clock: This is the number of half-moves since the last pawn
 * advance or capture.
 * 
 * 6. Full-move number: The number of the current full move.
 * 
 * @author gamo_ created at 23 may, 2019
 * 
 */
@Slf4j
//@SuppressWarnings({ "PMD.LawOfDemeter", "PMD.DataflowAnomalyAnalysis", "PMD.CommentSize" })
public final class FenUtils {

	private FenUtils() {
		// This is a utility class.
	}

	/**
	 * Given a FEN and a move, creates next FEN.
	 * 
	 * @return next FEN.
	 */
	public static String getNextFen(final String fen, final Move mov) {
		final Chessboard board = new Chessboard(fen);
		final Integer nextMove = MoveParserFromAN.buildMoveFromLAN(board, mov.getAnnotation());
		log.info("move: {}", mov.getPretty(board));
		board.makeMoveAndFlipTurn(nextMove);
		final String nextFen = board.toFenString();
		log.debug("nextFen: '{}'", nextFen);
		return nextFen;
	}

}
