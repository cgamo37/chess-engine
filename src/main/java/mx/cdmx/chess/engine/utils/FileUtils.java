package mx.cdmx.chess.engine.utils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author gamo_ - CGM
 *
 */
@SuppressWarnings({ "PMD.LawOfDemeter", "PMD.DataflowAnomalyAnalysis" })
@Slf4j
public final class FileUtils {

    private FileUtils() {
        // This is a utility class.
    }

    /**
     * Method to read file contents.
     * 
     * @param fileName: Path to file to read.
     * @return Content of text file as a String.
     * @throws IOException
     */
    public static String readFile(final String fileName) throws IOException {
        final Path filePath = Paths.get(fileName);
        try (Stream<String> content = Files.lines(filePath, StandardCharsets.UTF_8)) {
            final String asString = content.collect(Collectors.joining());
            log.trace("File content is:\n'{}'", asString);
            return asString;
        }
    }

    /**
     * Removes "go" command from game file and updates file with suffix, adding next
     * move calculated by chess engine.
     * 
     * @param absolutePath. Path to game's file.
     * @param suffix.       Movement in LAN to append as suffix. For instance 'e2e4'.
     * @throws IOException
     */
    public static void replace(final String absolutePath, final String suffix) throws IOException {
        final Path path = Paths.get(absolutePath);
        try (Stream<String> lines = Files.lines(path)) {
            final List<String> replaced = lines.map(line -> line.replace("go", "").concat(suffix))
                    .collect(Collectors.toList());
            Files.write(path, replaced);
        } catch (final NullPointerException e) {
            // This catch is needed because sometimes, editors add a final '\n' to files and
            // makes to fail this method of not handled in this way.
            // log.error("{} {}", e.toString());
        }
    }

}
