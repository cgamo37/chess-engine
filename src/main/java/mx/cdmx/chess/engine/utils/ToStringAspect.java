package mx.cdmx.chess.engine.utils;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.aop.aspectj.annotation.AspectJProxyFactory;

import com.github.louism33.chesscore.Chessboard;

import lombok.extern.slf4j.Slf4j;
import mx.cdmx.chess.engine.model.ChessboardWrapper;
import mx.cdmx.chess.engine.model.Move;

/**
 * 
 * @author gamo_ CGM at 24 may, 2019.
 *
 */
@SuppressWarnings({ "PMD.LawOfDemeter", "PMD.AtLeastOneConstructor" })
@Aspect
@Slf4j
public class ToStringAspect {

    /**
     * Utility method to 'decorate' board using this aspect.
     * 
     * @param board - Board to decorate.
     * @return Decorated proxy board.
     */
    public static ChessboardWrapper getProxy(final Chessboard board) {
        final AspectJProxyFactory proxyFactory = new AspectJProxyFactory(new ChessboardWrapper(board));
        proxyFactory.addAspect(ToStringAspect.class);
        return proxyFactory.getProxy();
    }

    /**
     * This method modifies rendering of chess-board without modifying underlying
     * implementation.
     */
    @Around("execution(* mx.cdmx.chess.engine.model.ChessboardWrapper.toString())")
    public Object aroundAdvice(final ProceedingJoinPoint joinPoint) {
        final ChessboardWrapper wrapper = (ChessboardWrapper) joinPoint.getTarget();
        final Chessboard chessboard = wrapper.getBoard();
        if (chessboard.isDrawByInsufficientMaterial()) {
            log.info("isDrawByInsufficientMaterial.");
        }
        return boardArt(chessboard);
    }

    /** My own version of 2 methods in com.github.louism33.chesscore.Art class. */
    /** Auxiliary method for rendering chess board in an easy, visual format. */
    private String boardArt(final Chessboard board) {
        final StringBuilder asString = new StringBuilder(512);
        asString.append("   a b c d e f g h\n  +---------------+\n");
        for (int y = 7; y >= 0; y--) {
            asString.append(y + 1).append(" |");
            for (int x = 7; x >= 0; x--) {
                asString.append(pieceByNumberASCII(board.pieceSquareTable[x + y * 8], x + y));
                if (x > 0) {
                    asString.append(' ');
                }
            }
            asString.append("| ").append(y + 1);
            asString.append('\n');
        }
        asString.append("  +---------------+\n   a b c d e f g h\n");

        return asString.toString();
    }

    // pieceCode: code for piece.
    // parity: it is odd (black) or even (white) square.
    @SuppressWarnings("PMD.AvoidFinalLocalVariable")
    private static String pieceByNumberASCII(final int pieceCode, final int parity) {
        final String pieceSymbol;
        if (1 <= pieceCode && pieceCode <= 12) {
            pieceSymbol = Move.MAP_TO_SYMBOL.get(pieceCode);
        } else {
            pieceSymbol = parity % 2 == 0 ? "·" : "=";
        }
        return pieceSymbol;
    }

}
