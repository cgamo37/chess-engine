package mx.cdmx.chess.engine.utils;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.List;

import com.github.louism33.chesscore.Chessboard;
import com.github.louism33.chesscore.MoveParser;
import com.github.louism33.utils.MoveParserFromAN;
import com.github.louism33.utils.PGNParser;
import com.rahul.stockfish.Stockfish;

import lombok.extern.slf4j.Slf4j;
import mx.cdmx.chess.engine.ChessEngine;
import mx.cdmx.chess.engine.axolotl.AxolotlEngine;
import uk.org.lidalia.sysoutslf4j.context.SysOutOverSLF4J;

/**
 * @see http://www.zoftino.com/java-file-system-watcher-example
 * @author gamo_
 *
 */
@SuppressWarnings({ "PMD.LawOfDemeter", "PMD.DataflowAnomalyAnalysis" })
@Slf4j
public final class FileWatcherUtils {
    /** Path to folder to watch. */
    private static final String FOLDER;
    /** Limit of time for 'thinking' for chess engine between each move. */
    private static final int TIME_LIMIT;
    /** Global board position. */
    private static Chessboard board;
    /** Counter for total moves in game. */
    private static int totalMoves;
    /**
     * Counter for number of file change events made: even -> write, odd -> read.
     */
    private static int parity;
    /** Chess engine that will make movements calculations. */
    private static final ChessEngine ENGINE;

    static {
        SysOutOverSLF4J.sendSystemOutAndErrToSLF4J();
        FOLDER = "/home/cesar/Dropbox/gymbo/project/personal/chess/";
        TIME_LIMIT = 30_000;
        board = new Chessboard();
        totalMoves = 1;
        if (true) {
            ENGINE = new AxolotlEngine();
        } else {
            ENGINE = new Stockfish();
        }
        ENGINE.setup();
    }

    private FileWatcherUtils() {
        // This is a utility class.
    }

    /**
     * Test method for tests. to do to move!
     * 
     * @param args
     * @throws IOException
     * @throws InterruptedException
     */
    public static void main(final String... args) throws IOException, InterruptedException {
        FileWatcherUtils.watchDirectory(FOLDER);
        ENGINE.stop();
    }

    /**
     * Utility method to watch a folder by name.
     * 
     * @param folder Path to folder to watch.
     * @throws IOException
     */
    public static void watchDirectory(final String folder) throws IOException, InterruptedException {
        final Path filePath = Paths.get(folder);
        final WatchService watchService = FileSystems.getDefault().newWatchService();
        // listen for modify events:
        filePath.register(watchService, StandardWatchEventKinds.ENTRY_MODIFY);
        while (true) {
            final WatchKey key = watchService.take();
            // Retrieves all the accumulated events:
            for (final WatchEvent<?> event : key.pollEvents()) {
                if (parity % 2 == 0) {
                    final Path path = (Path) event.context();
                    final String pathGame = folder + path.toString();
                    log.trace("File changed is '{}'", pathGame);
                    if (pathGame.contains("game.txt")) {
                        final String content = FileUtils.readFile(pathGame);
                        final String move = playChessGame(content);
                        FileUtils.replace(pathGame, move);
                    }
                }
                parity++;
            }
            key.reset();
        }

    }

    // 'content' argument is textual file content where game is been developed.
    private static String playChessGame(final String content) throws IOException, InterruptedException {
        final String lastMove = getLastMove(content);
        final String formerGame = content.replace("go", "");
        board = resetBoardFromPGN(formerGame);
        final String command = lastMove;
        final int move = computeMove(command);
        String newMove = null;
        if (move == 0) {
            log.warn("'{}' is not a legal move!", command);
            verifyCheck();
        } else {
            final String player = whoPlays();
            board.makeMoveAndFlipTurn(move);
            totalMoves++;
            newMove = MoveParser.toString(move);
            log.info("{} {} made move {}", player, totalMoves, newMove);
            log.info("Position:\n{}", ToStringAspect.getProxy(board));
        }
        return newMove;
    }

    private static String whoPlays() {
        String prompt;
        if (board.isWhiteTurn()) {
            prompt = "White";
        } else {
            prompt = "Black";
        }
        return prompt;
    }

    private static int computeMove(final String command) throws InterruptedException {
        int move;
        if ("go".equalsIgnoreCase(command)) {
            final String mov = ENGINE.calculateMove(board.toFenString(), TIME_LIMIT);
            move = MoveParserFromAN.buildMoveFromLAN(board, mov);
        } else {
            move = 0;
            final int[] moves = board.generateLegalMoves();
            if (moves.length == 0) {
                verifyCheck();
            }
            for (final int m : moves) {
                if (command.equals(MoveParser.toString(m))) {
                    move = m;
                    break;
                }
            }
        }
        return move;
    }

    private static Boolean verifyCheck() {
        final Boolean isInCheck = board.inCheck(board.isWhiteTurn());
        if (isInCheck) {
            log.info("Checkmate");
            // Starts a new game:
            board = new Chessboard();
            totalMoves = 1;
        } else {
            // Ahogado - it's a draw BUT CAN BE a mistake in annotations in file game.
            log.info("Stalemate?");
        }
        return isInCheck;
    }

    /**
     * 
     * @param game A description of a game, kind of: 1.c2c4 c7c5 2.etc.
     * @return
     */
    private static String getLastMove(final String game) {
        final String[] moves = game.split(" ");
        for (final String move : moves) {
            log.trace(move);
        }
        String lastMove = moves[moves.length - 1];
        if (lastMove.contains(".")) {
            lastMove = lastMove.substring(lastMove.lastIndexOf('.') + 1);
        }
        log.trace("lastMove: {}", lastMove);
        return lastMove.trim();
    }

    /**
     * Resets chess board to position indicated by PGN movements.
     * 
     * @param pgn
     * @return
     */
    public static Chessboard resetBoardFromPGN(final String pgn) {
        final Chessboard newBoard = new Chessboard();
        log.trace("{}", newBoard);
        final List<String> lans = PGNParser.parsePGNSimple(pgn);
        int index = 0;
        for (final String lan : lans) {
            index++;
            log.trace("{} {}", index, lan);
            final int move = MoveParserFromAN.buildMoveFromLAN(newBoard, lan);
            if (isLegalMovement(newBoard, move)) {
                newBoard.makeMoveAndFlipTurn(move);
                log.trace("{}", newBoard.toFenString());
                log.trace("{}", ToStringAspect.getProxy(newBoard));
            } else {
                final String message = lan + " is not a legal movement! index " + index;
                log.error("{}", message);
            }
        }
        log.debug("{}", ToStringAspect.getProxy(newBoard));
        return newBoard;
    }

    private static boolean isLegalMovement(final Chessboard newBoard, final int move) {
        final int[] legals = newBoard.generateLegalMoves();
        log.trace("move: {} legals:\n{}", move, legals);
        boolean isLegal = false;
        for (final int legal : legals) {
            if (legal == move) {
                isLegal = true;
                break;
            }
        }
        return isLegal;
    }

}
