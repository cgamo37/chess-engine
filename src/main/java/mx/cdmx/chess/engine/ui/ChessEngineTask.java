package mx.cdmx.chess.engine.ui;

import com.victorlaerte.asynctask.AsyncTask;

import lombok.extern.slf4j.Slf4j;
import mx.cdmx.chess.engine.ChessEngine;
import mx.cdmx.chess.engine.axolotl.AxolotlEngine;

/**
 * 
 * @author cesar 20 December, 2019 see
 *         https://www.developer.com/java/data/multithreading-in-javafx.html
 *
 */
@Slf4j
// <FEN, X, engineMove>
public class ChessEngineTask extends AsyncTask<String, Object, String> {
    /** Time left to engine to think it next movement. */
    private static final int TIME_LIMIT;
    /** Chess engine to play with. */
    private final ChessEngine engine;
    /** Needed to update UI. */
    private final ChessController controller;

    static {
        TIME_LIMIT = 10_000;
    }

    public ChessEngineTask(final ChessController controller) {
        engine = new AxolotlEngine();
        engine.setup();
        this.controller = controller;
    }

    /** {@inheritDoc} */
    @Override
    public void onPreExecute() {
        // TODO To setup progress bar or similar??

    }

    /** {@inheritDoc} */
    @Override
    public String doInBackground(final String... params) {
        final String fen = params[0];
        final String engineMove = engine.calculateMove(fen, TIME_LIMIT);
        log.debug("Engine move: {}", engineMove);
        return engineMove;
    }

    /** {@inheritDoc} */
    @Override
    public void onPostExecute(final String engineMove) {
        controller.doNextMove(engineMove);
    }

    /** {@inheritDoc} */
    @Override
    public void progressCallback(final Object... params) {
        // TODO To setup progress bar or similar??

    }

}
