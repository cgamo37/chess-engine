package mx.cdmx.chess.engine.ui;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.louism33.chesscore.Chessboard;

import lombok.extern.slf4j.Slf4j;
import mx.cdmx.chess.engine.model.ChessboardWrapper;
import mx.cdmx.chess.engine.utils.ToStringAspect;

@Controller
@Slf4j
public class ChessWebController {

    @GetMapping("/chess")
    public String start(final Model model) {
        // Used FEN is interesting in Marshall gambit.
        final Chessboard board = new Chessboard();
        final ChessboardWrapper proxy = ToStringAspect.getProxy(board);

        model.addAttribute("board", "\n" + proxy.toString());

        return "chess";
    }

    @PostMapping("/chess")
    public String doMove(@RequestParam(name="move", required=true) final String move, final Model model) {
        log.error("move: {}", move);
        return "chess";
    }

}
