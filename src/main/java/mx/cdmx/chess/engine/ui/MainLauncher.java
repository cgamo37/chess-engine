package mx.cdmx.chess.engine.ui;

import java.util.Date;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author César García Mauricio.
 *
 */
@SpringBootApplication
@Slf4j
public class MainLauncher {

    public static void main(final String[] args) {
        SpringApplication.run(MainLauncher.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner() {
        return args -> log.info("Starting Spring Boot App {}", new Date());
    }

}
