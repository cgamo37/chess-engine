package mx.cdmx.chess.engine.ui;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.github.louism33.chesscore.Chessboard;
import com.github.louism33.utils.MoveParserFromAN;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import mx.cdmx.chess.engine.model.ChessboardWrapper;
import mx.cdmx.chess.engine.model.Move;
import mx.cdmx.chess.engine.utils.ToStringAspect;

/**
 * 
 * @author CGM
 *
 */
@Slf4j
public class ChessController {
    /** Send movement from UI user to controller. */
    @FXML
    private TextField movement;
    /** Shows history and current board. */
    @FXML
    private Label messageLabel;
    /** Button for undo one move. */
    @FXML
    private Button back;
    /** Supporting chess board. */
    private final Chessboard board;
    /** Stores current game movements' history as string. */
    private StringBuilder history;
    /** Stores current game movements' history. */
    private final List<Move> moveHistory;
    /** Used to count the number of movements in game. */
    @Getter
    private int movCounter = 1;
    /** Play versus engine? */
    // Initially user *always* plays with white pieces.
    private boolean playEngine = true;

    public ChessController() {
        board = new Chessboard();
        history = new StringBuilder();
        moveHistory = new ArrayList<>();
    }

    public ChessController(final String history) {
        this.history = new StringBuilder(history);
        this.board = new Chessboard();
        this.moveHistory = new ArrayList<>();
        Integer count = 0;
        final String[] moves = history.split("\n");
        for (final String move : moves) {
            log.trace("{}", move);
            final String[] moveis = move.split(" ");
            for (final String movei : moveis) {
                log.trace("{}", movei);
            }
            count = Integer.valueOf(moveis[0].replaceFirst("\\.", ""));
            final String whiteMove = moveis[1];
            final String blackMove = moveis.length == 3 ? moveis[2] : null;

            board.makeMoveAndFlipTurn(MoveParserFromAN.buildMoveFromLAN(board, whiteMove));

            moveHistory.add(new Move(whiteMove));
            if (blackMove != null) {
                board.makeMoveAndFlipTurn(MoveParserFromAN.buildMoveFromLAN(board, blackMove));
                final Move bm = new Move(blackMove);
                moveHistory.add(bm);
            }
        }
        movCounter = count;
    }

    @FXML
    public void initialize() {
        final Image imageBack = new Image(getClass().getResourceAsStream("/images/back.png"));
        back.setGraphic(new ImageView(imageBack));
        ChessJavaFx.setController(this);
    }

    public void doMovement() {
        final String move = movement.getText();
        log.debug("Move: {}", move);
        movement.setText(null);

        if (Move.validate(board, move)) {
            back.setDisable(false);
            doNextMove(move); // user move
            if (playEngine) {
                // engine move:
                new ChessEngineTask(this).execute(board.toFenString());
            }
        } else {
            final StringBuilder errorMsg = new StringBuilder(move);
            errorMsg.append(" is not a valid move!!!");
            log.error(errorMsg.toString());
            movement.setText(errorMsg.toString());
        }
    }

    void doNextMove(final String move) {
        moveHistory.add(updateHistory(move));
        final Integer nextMove = MoveParserFromAN.buildMoveFromLAN(board, move);
        board.makeMoveAndFlipTurn(nextMove);
        log.trace("{}", board);
        final ChessboardWrapper proxy = ToStringAspect.getProxy(board);
        log.debug("{}\n{}", history, proxy);
        messageLabel.setText(proxy + "\n" + history);
    }

    private Move updateHistory(final String move) {
        if (board.isWhiteTurn()) {
            history.append(movCounter).append(". ");
        }
        final Move mov = new Move(move);
        history.append(mov.getPretty(board));
        if (!board.isWhiteTurn()) {
            history.append('\n');
            movCounter++;
        } else {
            history.append(' ');
        }
        return mov;
    }

    /**
     * Undo last movement in game.
     */
    public void doBack() {
        log.debug("Un doing last movement.");
        board.unMakeMoveAndFlipTurn();
        final String[] moves = history.toString().split(" ");
        for (final String move : moves) {
            log.trace("{}", move);
        }
        final String lastMove = moves[moves.length - 1];
        log.trace("lastMove: {}", lastMove);

        final int index = history.lastIndexOf(lastMove);

        if (index >= 0) {
            String newHist = history.substring(0, index).strip();
            if (newHist.endsWith(".")) {
                final int lastNewLine = newHist.lastIndexOf('\n');
                movCounter = Integer.valueOf(newHist.substring(lastNewLine + 1, newHist.length() - 1)) - 1;
                newHist = newHist.substring(0, lastNewLine + 1);
            }
            history = new StringBuilder(newHist);
            log.debug("New state:\n{}", this);
            if (messageLabel != null) {
                messageLabel.setText(history + "\n" + ToStringAspect.getProxy(board));
            }
            if (StringUtils.isBlank(newHist)) {
                back.setDisable(true);
            }
        }
    }

    /** {@inheritDoc} */
    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder(250);
        builder.append("ChessController [");
        if (board != null) {
            builder.append("board=\n");
            builder.append(ToStringAspect.getProxy(board));
            builder.append(", ");
        }
        if (history != null) {
            builder.append("history=\n");
            builder.append(history);
            builder.append(", ");
        }
        builder.append("movCounter=");
        builder.append(movCounter);
        builder.append(']');

        return builder.toString();
    }

}
