package mx.cdmx.chess.engine.ui;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lombok.extern.slf4j.Slf4j;
import uk.org.lidalia.sysoutslf4j.context.LogLevel;
import uk.org.lidalia.sysoutslf4j.context.SysOutOverSLF4J;

/**
 * 
 * @author CGM
 *
 */
@Slf4j
public class ChessJavaFx extends Application {
    // XXX NO me gusta usar variables globales como 'chessController'!!!
    private static ChessController chessController;
    private static final LogLevel LEVEL;
    private static final LogLevel ERROR_LEVEL;

    static {
        LEVEL = LogLevel.TRACE;
        ERROR_LEVEL = LogLevel.ERROR;
    }

    public static void main(final String... args) {
        SysOutOverSLF4J.sendSystemOutAndErrToSLF4J(LEVEL, ERROR_LEVEL);
        launch(args);
    }

    /** {@inheritDoc} */
    @Override
    public void start(final Stage stage) throws IOException {
        log.info("Starting chess application.");
        final String fxmlFile = "/fxml/chess-game.fxml";
        log.debug("Loading FXML for main view from: {}", fxmlFile);
        final FXMLLoader loader = new FXMLLoader();
        final Parent rootNode = loader.load(getClass().getResourceAsStream(fxmlFile));

        log.debug("Showing JFX scene");
        final Scene scene = new Scene(rootNode, 600, 1000);
        scene.getStylesheets().add("/styles/styles.css");
        stage.setScene(scene);
        stage.setOnCloseRequest(event -> log.info("Final position is:\n{}", chessController));
        stage.setTitle("Chess JavaFX");
        stage.show();
    }

    public static void setController(final ChessController chessController) {
        ChessJavaFx.chessController = chessController;
    }

}
