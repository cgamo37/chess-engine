package mx.cdmx.chess.engine.axolotl;

import static com.github.louism33.axolotl.search.EngineSpecifications.MAX_THREADS;

import org.apache.commons.lang3.NotImplementedException;

import com.github.louism33.axolotl.search.Engine;
import com.github.louism33.axolotl.search.SearchSpecs;
import com.github.louism33.chesscore.Chessboard;
import com.github.louism33.chesscore.MoveParser;

import mx.cdmx.chess.engine.ChessEngine;

/**
 * @created Saturday 01 June 2019
 * @author gamo_ CGM
 *
 */
public class AxolotlEngine implements ChessEngine {
    /** Underlying engine. */
    private final Engine engine;
    /** Time for machine to 'think' in milliseconds. */
    private static final long TIME_LIMIT = 10_000;

    /** Kind of set-up. */
    public AxolotlEngine() {
        engine = new Engine();
        SearchSpecs.basicTimeSearch(TIME_LIMIT);
        Engine.setThreads(MAX_THREADS);
    }

    /** {@inheritDoc} */
    @Override
    public Boolean setup() {
        Engine.resetFull();
        return true;
    }

    /** {@inheritDoc} */
    @Override
    public void stop() {
        throw new NotImplementedException("This method is not needed for this engine");
    }

    /** {@inheritDoc} */
    @Override
    public String calculateMove(final String fen, final int timeLimit) {
        final Chessboard board = new Chessboard(fen);
        final int move = engine.simpleSearch(board);
        return MoveParser.toString(move);
    }

}
