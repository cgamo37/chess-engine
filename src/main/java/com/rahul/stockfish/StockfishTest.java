package com.rahul.stockfish;

import java.util.ResourceBundle;
import java.util.Scanner;

import com.github.louism33.chesscore.Chessboard;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import mx.cdmx.chess.engine.model.ChessboardWrapper;
import mx.cdmx.chess.engine.model.Move;
import mx.cdmx.chess.engine.utils.ChessUtils;
import mx.cdmx.chess.engine.utils.FenUtils;
import mx.cdmx.chess.engine.utils.ToStringAspect;

/**
 * 
 * @author gamo_ & . May 2019.
 * Modified at 20 December 2019: adding static class for returning move + status (game over boolean).
 *
 */
@Slf4j
public class StockfishTest {
	/** This instance makes requests to engine via UCI commands. */
	private static Stockfish client;
	/** Maximum number of movements by game. */
	private static final int MAX = 250;
	/** Time for thinking (milliseconds). */
	private static final int TIME = 2000;
	private static final boolean FISH_IS_WHITE;
	/** Auxiliary to read input from standard input: human player commands. */
	private static final Scanner in = new Scanner(System.in);
	/** Enables auto-play: Stockfish versus Stockfish. */
	private static final Boolean IS_SELF_PLAY;

	static {
		final ResourceBundle resources = ResourceBundle.getBundle("chess-engine");
		IS_SELF_PLAY = Boolean.valueOf(resources.getString("stockfish_self_play"));
		FISH_IS_WHITE = Boolean.valueOf(resources.getString("stockfish_is_white"));
		log.info("Fish is white? {}", FISH_IS_WHITE);
		try {
			client = setup();
		} catch (final InterruptedException e) {
			log.error("", e);
		}
	}

	public static void main(final String... args) throws InterruptedException {
		String fen = ChessUtils.STD_FEN;
		boolean isWhiteTurn = false;

		for (int i = 0; i < MAX; i++) {
			final MoveStatus mov = getNextMove(isWhiteTurn, fen);
			if (mov.getIsGameOver()) {
				break;
			}
			final String move = mov.getMove().getAnnotation();
			log.debug("#{} Move : '{}'\nFEN: {}", i, move, fen);
			fen = FenUtils.getNextFen(fen, mov.getMove());
			isWhiteTurn = !isWhiteTurn;
            logState(new Chessboard(fen), i);
		}

		log.debug("Stopping engine..");
		client.stop();
		in.close();
	}

	private static MoveStatus getNextMove(final boolean isWhiteTurn, final String fen) throws InterruptedException {
		MoveStatus mov = null;

		if (/* fishIsWhite && */ isWhiteTurn) {
			mov = getNextMoveByStockfish(fen);
		} else {
			if (IS_SELF_PLAY) {
				mov = getNextMoveByStockfish(fen);
			} else {
				final String move = in.nextLine();
				mov = new MoveStatus(new Move(move), false);
			}
		}

		return mov;
	}

	private static MoveStatus getNextMoveByStockfish(final String fen) throws InterruptedException {
		Boolean gameIsOver = false;
		final String move = client.calculateMove(fen, TIME);
		if ("(none)\n".equals(move)) {
			log.info("Game is over!");
			gameIsOver = true;// break!
//			System.exit(0);
		}
		return new MoveStatus(new Move(move), gameIsOver);
	}

	@Data
	private static class MoveStatus {
		private final Move move;
		private final Boolean isGameOver;
	}

	private static Stockfish setup() throws InterruptedException {
		final Stockfish client = new Stockfish();
		if (Boolean.TRUE.equals(client.setup())) {
			log.debug("Engine has started..");
		} else {
			log.debug("Oops! Something went wrong..");
		}

		// send commands manually
		client.sendCommand("uci");

		// receive output dump
		log.debug(client.getOutput(0));
		return client;
	}

	private static void logState(final Chessboard board, final Integer moveNum) {
		final ChessboardWrapper proxy = ToStringAspect.getProxy(board);
		log.info("Board state {}\n{}:", moveNum, proxy);
	}

}
