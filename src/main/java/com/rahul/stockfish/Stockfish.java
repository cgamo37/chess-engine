package com.rahul.stockfish;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ResourceBundle;

import org.apache.commons.lang3.NotImplementedException;

import lombok.extern.slf4j.Slf4j;
import mx.cdmx.chess.engine.ChessEngine;

/**
 * A simple and efficient client to run Stockfish from Java
 * 
 * @author Rahul A R, CGM.
 * 
 */
@Slf4j
public class Stockfish implements ChessEngine {

    private BufferedReader processReader;
    private OutputStreamWriter processWriter;

    /**
     * Starts Stockfish engine as a process and initializes it.
     * 
     * @return True on success. False otherwise
     */
    @Override
    public Boolean setup() {
        try {
            final ResourceBundle resources = ResourceBundle.getBundle("chess-engine");
            final Process engineProcess = Runtime.getRuntime().exec(resources.getString("stockfish_path"));
            processReader = new BufferedReader(new InputStreamReader(engineProcess.getInputStream()));
            processWriter = new OutputStreamWriter(engineProcess.getOutputStream());
        } catch (final IOException e) {
            return false;
        }
        return true;
    }

    /**
     * Takes in any valid UCI command and executes it.
     * 
     * @param command
     */
    public void sendCommand(final String command) {
        try {
            processWriter.write(command + "\n");
            processWriter.flush();
        } catch (final IOException e) {
            log.error("", e);
        }
    }

    /**
     * This is generally called right after 'sendCommand' for getting the raw output
     * from Stockfish.
     * 
     * @param waitTime Time in milliseconds for which the function waits before
     *                 reading the output. Useful when a long running command is
     *                 executed
     * @return Raw output from Stockfish.
     * @throws InterruptedException
     */
    public String getOutput(final int waitTime) throws InterruptedException {
        final StringBuilder buffer = new StringBuilder();
        try {
            Thread.sleep(waitTime);
            sendCommand("isready");
            while (true) {
                final String text = processReader.readLine();
                if (text.equals("readyok")) {
                    break;
                } else {
                    buffer.append(text + "\n");
                }
            }
        } catch (final IOException e) {
            log.error("", e);
        }

        final String result = buffer.toString();
        log.trace("result:\n{}", result);
        return result;
    }

    /**
     * This function returns the best move for a given position after calculating
     * for 'waitTime' milliseconds.
     * 
     * @param fen      Position string.
     * @param waitTime in milliseconds
     * @return Best Move in PGN format
     * @throws InterruptedException
     */
    public String calculateMove(final String fen, final int waitTime) {
        sendCommand("position fen " + fen);
        sendCommand("go movetime " + waitTime);
        // cycle: if time slice is not enough for move computation, wait more time and
        // try again later:
        String out = null;
        int i = 1;
        do {
            try {
                out = getOutput(i * waitTime + 20);
            } catch (final InterruptedException e) {
                throw new NotImplementedException(e);
            }
            i++;
            log.trace("time {}", i);
        } while (!out.contains("bestmove "));
        return out.split("bestmove ")[1].split(" ")[0];
    }

    /**
     * Stops Stockfish and cleans up before closing it
     */
    @Override
    public void stop() {
        try {
            sendCommand("quit");
            processReader.close();
            processWriter.close();
        } catch (final IOException e) {
            log.error("", e);
        }
    }

    /**
     * Get a list of all legal moves from the given position
     * 
     * @param fen Position string
     * @return String of moves
     * @throws InterruptedException
     */
    public String getLegalMoves(final String fen) throws InterruptedException {
        sendCommand("position fen " + fen);
        sendCommand("d");
        return getOutput(0).split("Legal moves: ")[1];
    }

}
